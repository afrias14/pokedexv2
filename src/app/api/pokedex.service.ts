import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const nameAPI = 'https://pokeapi.co/api/v2';

@Injectable({
  providedIn: 'root'
})
export class PokedexService {

  constructor(public http: HttpClient) {
    console.log('API excuted');
  }

  getPokedex(dexId): any {
      return this.http.get<any>(nameAPI + '/pokedex/' + dexId);
  }

  getPokemonType(entry_number): any {
    return this.http.get<any>(nameAPI + '/pokemon/' + entry_number)
  }

  getAllPokemon(): any {
      return this.http.get<any>(nameAPI + '/pokemon');
  }


}
