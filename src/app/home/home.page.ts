import { Component } from '@angular/core';
import { PokedexService } from '../api/pokedex.service';
import { LoadingController } from "@ionic/angular";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public types: Array<any> = [];
  public dexId: any;
  public pkmList: Array<any> = [];
  public pkmType: Array<any> = [];
  public id: number;
  public sortedarray: any;
  searchQuery: string = '';

  constructor(public api: PokedexService, public loadingController: LoadingController) {
      this.dexId = 2;
      // this.getPokedex();


  }

  ngOnInit() {
    console.log('INIT');

    // let loader = this.loadingController.create({});
    this.presentLoading();

  }


    async presentLoading() {
        const loading = await this.loadingController.create({
            message: 'Please wait!',
            duration: 3000
        });
        return await loading.present().then(() => {
            this.getPokedex()
        });
    }

  getPokedex() {

      this.api.getPokedex(this.dexId).subscribe(pokedex => {

          pokedex.pokemon_entries.map(pokemon => {
              // console.log(pokemon);

              this.api.getPokemonType(pokemon.entry_number).subscribe(type => {

                  this.pkmType = [];
                  this.sortedarray = [];
                  for (let i = 0; i < type.types.length; i++) {
                      this.pkmType.push(type.types[i].type.name);
                  }

                  this.pkmList.push({entry_number: pokemon.entry_number ,name: pokemon.pokemon_species.name, types:  this.pkmType});

                  this.sortedarray = this.sortByKey(this.pkmList, 'entry_number');

                  // console.log(this.pkmType);
              });
          })

      })

  }

    public sortByKey(array, key) {
        return array.sort(function (a, b) {
            var x = a[key]; var y = b[key];
            return ((x < y) ? -1 : ((x > y) ? 0 : 1));
        });
    }

    getColor(type) {
      switch (type) {
          case "normal":
              return "#aa9";
          case "fighting":
              return "#b54";
          case "flying":
              return "#89f";
          case "poison":
              return "#a59";
          case "ground":
              return "#db5";
          case "rock":
              return "#ba6";
          case "bug":
              return "#ab2";
          case "ghost":
              return "#66b";
          case "steel":
              return "#aab";
          case "fire":
              return "#f42";
          case "water":
              return "#39f";
          case "grass":
              return "#7c5";
          case "electric":
              return "#fc3";
          case "psychic":
              return "#f59";
          case "ice":
              return "#6cf";
          case "dragon":
              return "#76e";
          case "dark":
              return "#754";
          case "fairy":
              return "#e9e";
      }
    }

    getItems(ev: any) {

      const val = ev.target.value;

      if(val && val.trim() != '') {
          this.pkmList = this.pkmList.filter((item) => {
              return item;
          })
      }

    }

}
