export class Pokemon {

    private _entry_number: any;


    constructor(entry_number: any) {
        this._entry_number = entry_number;
    }

    get entry_number(): any {
        return this._entry_number;
    }

    set entry_number(value: any) {
        this._entry_number = value;
    }
}